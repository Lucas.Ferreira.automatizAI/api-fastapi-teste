<div align="center"><h1>API TREINO FASTAPI 🖥️</h1></div>


### ✒️ Introdução
Api solicitada para treino, possui um crud de pessoas muito simples, feito em menos de uma hora.

### 🔌 Instalação da Aplicação
Para instalar as bibliotecas python é recomendado criar um ambiente virtual.
Dentro do ambiente virtual, utilize o comando:
> pip install -r requeriments.txt

### ⚙️ Variáveis de ambiente
Não se aplica no momento

### 📀 Iniciar Aplicação
> sudo docker build -t api-teste . --no-cache
> docker run -p 8080:8000 api-teste
### 🧪 Executar Testes
Ainda não possui testes

### 🛠️ Ferramentas Utilizadas
* [Python](https://www.python.org/)
* [FastAPI](https://fastapi.tiangolo.com/)


### 🧔 Responsáveis pelo projeto
👨[Lucas Ferreira](mailto:lucas.ferreira@funcionarios.automatizai.com.br)

<div align="center"><img width="500" alt="Logo" src="https://automatizai.com.br/wp-content/uploads/2020/07/Automatizai-horizontal.png"></div>