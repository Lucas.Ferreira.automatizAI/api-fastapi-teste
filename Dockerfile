FROM python:3.7.0

COPY app /api/app
COPY mock.json /api/mock.json

WORKDIR api/

RUN pip3 install fastapi uvicorn

EXPOSE 8000

CMD ["uvicorn", "app:app", "--host","0.0.0.0", "--port", "8000"]