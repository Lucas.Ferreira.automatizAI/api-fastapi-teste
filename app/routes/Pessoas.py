from fastapi import APIRouter, HTTPException, status
from app.database.Pessoas import MockDatabasePessoas
from app.models.Pessoa import Pessoa, UpdatePessoa
database = MockDatabasePessoas()

router = APIRouter()

@router.get('/')
def root():
    """
    **Traz uma verdade sobre essa empresa perante as demais**
    """
    return {"AutomatizAI": "Todo poder pertence aos Camaleões, nem tenta competir."}

@router.get('/pessoas')
def pessoas():
    """
    Recupera todos os registro do banco.
    """
    return database.Pessoas()

@router.post('/pessoa', status_code=status.HTTP_201_CREATED, response_model=Pessoa)
def cadastra_pessoas(pessoa : Pessoa):
    """
    Cadastra um registro de Pessoa no Banco com os seguintes campos:

    - **id**: Identificador numerico do registro;
    - **nome**: Nome da pessoa;
    - **email**: Email da Pessoa;
    - **cpf**: Cpf da Pessoa;
    - **ativo**: Status da pessoa na plataforma;

    **Todos os campos são obrigatórios**
    """

    if database.Adiciona(pessoa):
        return {"message": "sucess"}

@router.get('/pessoa/{id}')
def recupera_pessoa(id : int):
    """
    Recupera um registro de Pessoa no Banco com os seguintes campos:

    - **id**: Identificador numerico do registro;
    - **nome**: Nome da pessoa;
    - **email**: Email da Pessoa;
    - **cpf**: Cpf da Pessoa;
    - **ativo**: Status da pessoa na plataforma;

    """
    pessoa = database.RecuperaUm(id)
    if pessoa:
        return pessoa
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Item not found")

@router.delete('/pessoa/{id}')
def remove_pessoa(id : int):
    """
    Apaga um registro de Pessoa no Banco, caso ela exista.
    """


    if database.Remove(id):
        return {"message": "sucess"}
    else:
        raise HTTPException(status_code=404, detail="Item not found")

@router.put('/pessoa/{id}')
def atualiza_pessoa(id : int, pessoa : UpdatePessoa):

    """
    Atualiza os campos de um registro de Pessoa no Banco:

    - **id**: Identificador numerico do registro;
    - **nome**: Nome da pessoa;
    - **email**: Email da Pessoa;
    - **cpf**: Cpf da Pessoa;
    - **ativo**: Status da pessoa na plataforma;

    **OBS**: Os campos são todos opcionais.

    """
    pessoa = database.Atualiza(id, pessoa)
    if pessoa:
        return pessoa
    else:
        raise HTTPException(status_code=404, detail="Item not found")