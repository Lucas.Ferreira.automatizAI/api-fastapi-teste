import json
from app.models.Pessoa import Pessoa, UpdatePessoa

class MockDatabasePessoas:
    _Pessoas = []
    _nome_arquivo = ''
    
    def __init__(self, nome_arquivo='mock.json'):
        self._nome_arquivo = nome_arquivo
    
    def _abre_mock(self):
        with open(self._nome_arquivo, 'r') as mock_json:
            return json.load(mock_json)

    def _atualiza_mock(self):
        with open(self._nome_arquivo, 'w') as mock_json:
            json.dump(self._Pessoas, mock_json)

    def Pessoas(self):
        self._Pessoas = self._abre_mock()
        return self._Pessoas

    def Adiciona(self, pessoa : Pessoa):
        self._Pessoas.append({
            "id": pessoa.id,
            "nome": pessoa.nome, 
            "email": pessoa.email, 
            "cpf": pessoa.cpf, 
            "ativo": pessoa.ativo, 
        })
        self._atualiza_mock()
        return True
    
    def Remove(self, id : int):
        remover = self.RecuperaUm(id)
        if remover:
            self._Pessoas.remove(remover)
            self._atualiza_mock()
            return True
        return False

    def RecuperaUm(self, id : int):
        for item in self._Pessoas:
            if item['id'] == id:
                return item
        return None
    
    def Atualiza(self, id : int, pessoa: UpdatePessoa):
        atualizar = self.RecuperaUm(id)

        if atualizar:
            nova_pessoa = atualizar.copy()
            
            for chave, valor in pessoa:
                if valor:
                    nova_pessoa[chave] = valor
            
            self._Pessoas[self._Pessoas.index(atualizar)] = nova_pessoa
            self._atualiza_mock()
            return nova_pessoa
        return None