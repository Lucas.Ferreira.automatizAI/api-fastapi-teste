from pydantic import BaseModel, Field
from typing import Optional

class Pessoa(BaseModel):
    """
    Modelo que simboliza Pessoa Fisica
    """
    id: int = Field(..., description="Numero que identifica a pessoa no mock", example=0)
    nome: str = Field(..., description="Nome da Pessoa", example="Fredinho, o Camaleão")
    email: str = Field(..., description="Email da Pessoa", example="Fredinho.Camaleão@funcionarios.automatizai.com.br")
    cpf: str = Field(..., description="CPF da pessoa", example="36512356478")
    ativo: bool = Field(..., description="Situação da Pessoa", example=True)

class UpdatePessoa(BaseModel):
    id:  Optional[int]
    nome: Optional[str]
    email: Optional[str]
    cpf: Optional[str]
    ativo: Optional[bool]


