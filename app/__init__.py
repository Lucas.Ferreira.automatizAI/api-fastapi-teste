from fastapi import FastAPI
from app.routes import Pessoas


app = FastAPI(
    title="FastApi inicial AUTOMATIZAI",
    description="Api Rest de treino",
    version="1.0.0"
)

app.include_router(Pessoas.router)